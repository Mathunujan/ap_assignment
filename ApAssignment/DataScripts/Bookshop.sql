CREATE DATABASE  IF NOT EXISTS `book` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `book`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: book
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Title` varchar(30) DEFAULT NULL,
  `ISBN` varchar(30) DEFAULT NULL,
  `author` varchar(100) NOT NULL,
  `publisher` varchar(100) NOT NULL,
  `quantity` int(10) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Price` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `callno` (`Title`),
  UNIQUE KEY `callno_2` (`Title`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,'A@4','C In Depth','Shrivastav','BPB',2,'2016-07-19 19:37:56',NULL),(2,'B@1','DBMS','Korth','Pearson',3,'2016-07-18 18:39:52',NULL),(3,'G@12','Let\'s see','Yashwant Kanetkar','BPB',10,'2016-07-18 23:02:14',NULL),(4,'core Java','8tjkl','Anton','Pearson',5,'2019-04-16 18:02:41',50),(5,'.net','omg78','Leon','Bcas',33,'2019-04-17 04:45:07',50),(6,'networking','t78m','Leon','NexGen',50,'2019-04-05 17:55:55',50),(7,'web application','k48m','Kajan','NexGen',58,'2019-04-05 18:04:25',70);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issuebooks`
--

DROP TABLE IF EXISTS `issuebooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issuebooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `BookTitle` varchar(30) DEFAULT NULL,
  `customerId` varchar(30) DEFAULT NULL,
  `CustomerName` varchar(30) DEFAULT NULL,
  `Date_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Quantity` int(11) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issuebooks`
--

LOCK TABLES `issuebooks` WRITE;
/*!40000 ALTER TABLE `issuebooks` DISABLE KEYS */;
INSERT INTO `issuebooks` VALUES (4,'A@4','23','kk','2016-07-19 18:43:16',NULL,NULL),(6,'A@4','335','Sumedh','2016-07-19 18:44:34',NULL,NULL),(7,'A@4','87','abhishek','2016-07-19 18:46:12',NULL,NULL),(8,'1','','mathunujan','2019-03-24 17:39:12',5,50),(9,'core java','','mathunujan','2019-03-25 07:40:43',5,50),(10,'core java','','mathunujan','2019-03-25 08:04:02',2,50),(11,'core java','','mathunujan','2019-04-04 15:56:10',1,50),(12,'core java','','mathunujan','2019-04-04 17:49:04',5,60),(13,'core java','','mathunujan','2019-04-04 17:49:14',5,60),(14,'core java','','mathunujan','2019-04-05 07:07:35',5,60),(15,'core java','','mathunujan','2019-04-05 16:52:10',1,0),(16,'core java','','mathunujan','2019-04-05 16:53:47',1,1),(17,'core java','','mathunujan','2019-04-05 16:56:20',5,1),(18,'core java','','mathunujan','2019-04-05 17:43:48',4,50),(19,'.net','','mathunujan','2019-04-05 17:53:33',1,50),(20,'web application','','mathunujan','2019-04-05 18:02:42',4,70),(21,'web application','','mathunujan','2019-04-05 18:04:25',2,140),(22,'1','','mathunujan','2019-04-09 09:40:07',1,50),(23,'1','','mathunujan','2019-04-09 09:40:31',3,50),(24,'1','','mathunujan','2019-04-09 09:42:08',2,100),(25,'1','','mathunujan','2019-04-09 10:04:44',1,50),(26,'1','',NULL,'2019-04-13 08:47:36',2,100),(27,'1','','thilagshana','2019-04-13 08:50:32',2,100),(28,'1','','thilagshana','2019-04-13 12:11:04',2,100),(29,'1','','thilagshana','2019-04-13 12:14:44',3,150),(30,'1','','thilagshana','2019-04-13 12:17:58',1,50),(31,'1','','mathunujan','2019-04-13 12:30:38',1,50),(32,'1','','thilagshana','2019-04-13 12:33:31',1,50),(33,'1','','thilagshana','2019-04-13 12:35:38',1,50),(34,'1','','thilagshana','2019-04-13 12:51:09',1,50),(35,'1','','thilagshana','2019-04-16 17:47:51',1,50),(36,'1','','thilagshana','2019-04-16 17:49:47',1,50),(37,'1','','thilagshana','2019-04-16 18:01:56',1,50),(38,'1','','thilagshana','2019-04-16 18:02:41',2,100),(39,'.net','','thilagshana','2019-04-17 04:45:07',1,50);
/*!40000 ALTER TABLE `issuebooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `librarian`
--

DROP TABLE IF EXISTS `librarian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `librarian` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `librarian`
--

LOCK TABLES `librarian` WRITE;
/*!40000 ALTER TABLE `librarian` DISABLE KEYS */;
INSERT INTO `librarian` VALUES (1,'Prabhakar','ppp','prabhakar@gmail.com','javatpoint','noida','9998328238'),(4,'sumedh','sumesh','sumesh@gmail.com','Kuch Bhi','noida','93823932823'),(6,'abhi','abhi','abhi@gmail.com','javatpoint','noida','92393282323'),(7,'mathunujan','mathu123','smathunujan@gmail.com','kokuvil','Jaffna','0779980832'),(8,'Thilagshana','smiley','thilagshana1998@gmail.com','Kondavil','Jaffna','0764853194');
/*!40000 ALTER TABLE `librarian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `OId` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerName` varchar(50) DEFAULT NULL,
  `BookTitle` varchar(50) DEFAULT NULL,
  `DelAddress` varchar(50) DEFAULT NULL,
  `DelCity` varchar(50) DEFAULT NULL,
  `State` varchar(50) DEFAULT NULL,
  `PostCode` varchar(50) DEFAULT NULL,
  `Country` varchar(50) DEFAULT NULL,
  `TPrice` double DEFAULT NULL,
  PRIMARY KEY (`OId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'thilagshana','vsfv','kokuvil','jaffna','northern','456','456',50),(2,'thilagshana','1','kondavil','jaffna','northern','456','456',100),(3,'thilagshana','1','kokuvil','jaffna','northern','456','456',100);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `shoppingcart`
--

DROP TABLE IF EXISTS `shoppingcart`;
/*!50001 DROP VIEW IF EXISTS `shoppingcart`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `shoppingcart` AS SELECT 
 1 AS `id`,
 1 AS `Title`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `shoppingt`
--

DROP TABLE IF EXISTS `shoppingt`;
/*!50001 DROP VIEW IF EXISTS `shoppingt`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `shoppingt` AS SELECT 
 1 AS `id`,
 1 AS `Title`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `shoppingcart`
--

/*!50001 DROP VIEW IF EXISTS `shoppingcart`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `shoppingcart` AS select `librarian`.`id` AS `id`,`books`.`Title` AS `Title` from (`librarian` join `books`) where (`books`.`id` = 10) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `shoppingt`
--

/*!50001 DROP VIEW IF EXISTS `shoppingt`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `shoppingt` AS select `librarian`.`id` AS `id`,`books`.`Title` AS `Title` from (`librarian` join `books`) where (`books`.`id` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-18 22:38:12
