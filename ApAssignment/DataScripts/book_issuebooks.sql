-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: book
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `issuebooks`
--

DROP TABLE IF EXISTS `issuebooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issuebooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `BookTitle` varchar(30) DEFAULT NULL,
  `customerId` varchar(30) DEFAULT NULL,
  `CustomerName` varchar(30) DEFAULT NULL,
  `Date_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Quantity` int(11) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issuebooks`
--

LOCK TABLES `issuebooks` WRITE;
/*!40000 ALTER TABLE `issuebooks` DISABLE KEYS */;
INSERT INTO `issuebooks` VALUES (4,'A@4','23','kk','2016-07-19 18:43:16',NULL,NULL),(6,'A@4','335','Sumedh','2016-07-19 18:44:34',NULL,NULL),(7,'A@4','87','abhishek','2016-07-19 18:46:12',NULL,NULL),(8,'1','','mathunujan','2019-03-24 17:39:12',5,50),(9,'core java','','mathunujan','2019-03-25 07:40:43',5,50),(10,'core java','','mathunujan','2019-03-25 08:04:02',2,50),(11,'core java','','mathunujan','2019-04-04 15:56:10',1,50),(12,'core java','','mathunujan','2019-04-04 17:49:04',5,60),(13,'core java','','mathunujan','2019-04-04 17:49:14',5,60),(14,'core java','','mathunujan','2019-04-05 07:07:35',5,60),(15,'core java','','mathunujan','2019-04-05 16:52:10',1,0),(16,'core java','','mathunujan','2019-04-05 16:53:47',1,1),(17,'core java','','mathunujan','2019-04-05 16:56:20',5,1),(18,'core java','','mathunujan','2019-04-05 17:43:48',4,50),(19,'.net','','mathunujan','2019-04-05 17:53:33',1,50),(20,'web application','','mathunujan','2019-04-05 18:02:42',4,70),(21,'web application','','mathunujan','2019-04-05 18:04:25',2,140),(22,'1','','mathunujan','2019-04-09 09:40:07',1,50),(23,'1','','mathunujan','2019-04-09 09:40:31',3,50),(24,'1','','mathunujan','2019-04-09 09:42:08',2,100),(25,'1','','mathunujan','2019-04-09 10:04:44',1,50),(26,'1','',NULL,'2019-04-13 08:47:36',2,100),(27,'1','','thilagshana','2019-04-13 08:50:32',2,100),(28,'1','','thilagshana','2019-04-13 12:11:04',2,100),(29,'1','','thilagshana','2019-04-13 12:14:44',3,150),(30,'1','','thilagshana','2019-04-13 12:17:58',1,50),(31,'1','','mathunujan','2019-04-13 12:30:38',1,50),(32,'1','','thilagshana','2019-04-13 12:33:31',1,50),(33,'1','','thilagshana','2019-04-13 12:35:38',1,50),(34,'1','','thilagshana','2019-04-13 12:51:09',1,50),(35,'1','','thilagshana','2019-04-16 17:47:51',1,50),(36,'1','','thilagshana','2019-04-16 17:49:47',1,50),(37,'1','','thilagshana','2019-04-16 18:01:56',1,50),(38,'1','','thilagshana','2019-04-16 18:02:41',2,100),(39,'.net','','thilagshana','2019-04-17 04:45:07',1,50);
/*!40000 ALTER TABLE `issuebooks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-18 23:05:40
