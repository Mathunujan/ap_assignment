import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CardPayForm extends JFrame {
	static CardPayForm frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JLabel lblCriditcardNo;
	private JTextField textField_3;
	private JLabel lblSecurityCode;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new CardPayForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CardPayForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(124, 14, 254, 28);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(234, 78, 86, 28);
		contentPane.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(124, 78, 86, 28);
		contentPane.add(textField_2);

		JLabel lblNewLabel = new JLabel("Expiration Date");
		lblNewLabel.setBounds(22, 85, 92, 14);
		contentPane.add(lblNewLabel);

		lblCriditcardNo = new JLabel("Cridit card No");
		lblCriditcardNo.setBounds(10, 21, 92, 14);
		contentPane.add(lblCriditcardNo);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(128, 145, 192, 28);
		contentPane.add(textField_3);

		lblSecurityCode = new JLabel("Security Code");
		lblSecurityCode.setBounds(35, 152, 67, 14);
		contentPane.add(lblSecurityCode);

		btnNewButton = new JButton("Submit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OrderAddForm.main(new String[] {});
				frame.dispose();
			}
		});
		btnNewButton.setBounds(231, 202, 89, 23);
		contentPane.add(btnNewButton);
	}
}
