import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class OrderAddForm extends JFrame {
	static OrderAddForm frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new OrderAddForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OrderAddForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 521);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(175, 35, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(175, 88, 86, 20);
		contentPane.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(175, 136, 86, 20);
		contentPane.add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(175, 192, 86, 20);
		contentPane.add(textField_3);

		JLabel lblNewLabel = new JLabel("Delivery Address");
		lblNewLabel.setBounds(84, 38, 86, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Delivery city");
		lblNewLabel_1.setBounds(105, 91, 74, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblStatecountry = new JLabel("State/Country");
		lblStatecountry.setBounds(105, 139, 74, 14);
		contentPane.add(lblStatecountry);

		JLabel lblPostalCode = new JLabel("Postal code");
		lblPostalCode.setBounds(105, 195, 64, 14);
		contentPane.add(lblPostalCode);

		JLabel lblCountry = new JLabel("Country");
		lblCountry.setBounds(105, 243, 46, 14);
		contentPane.add(lblCountry);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(175, 240, 86, 20);
		contentPane.add(comboBox);

		JButton btnNewButton = new JButton("Save & Continue");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String dAddress = textField.getText();
				String dCity = textField_1.getText();
				String state = textField_2.getText();
				String postCode = textField_3.getText();
				String country = textField_3.getText();

				// Double Tprice = Double.parseDouble(textField.getText());
				int i = OrderDao.save(dAddress, dCity, state, postCode, country);
				if (i > 0) {
					BuyBookDao.delete(ShopCartView.purchaseId);
					JOptionPane.showMessageDialog(OrderAddForm.this,
							"Your order added successfully!\n and ready to shipping process");
					InvoiceOrder.main(new String[] {});
					frame.dispose();

				} else {
					JOptionPane.showMessageDialog(OrderAddForm.this, "Sorry, unable to save!");
				}

			}
		});
		btnNewButton.setBounds(231, 292, 132, 23);
		contentPane.add(btnNewButton);
	}
}
