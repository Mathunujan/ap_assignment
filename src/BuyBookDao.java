import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mysql.cj.xdevapi.Statement;

public class BuyBookDao {
	static int cId;

	public static void delete(int bookId) {
		boolean status = false;

		try {
			Connection con = DB.getConnection();

			status = updatebook(bookId);// updating quantity and issue

			if (status) {

				PreparedStatement ps = con.prepareStatement("delete from issuebooks where id=?");
				ps.setInt(1, bookId);
				ResultSet rs = ps.executeQuery();
				// ps.setInt(2, customerId);
				// ResultSet rs = ps.executeQuery();
				// status = rs.next();
				con.close();
			}

			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static boolean updatebook(int bookId) {
		boolean status = false;
		try {
			Connection con = DB.getConnection();

			PreparedStatement ps = con.prepareStatement("select id,CustomerName from issuebooks where id=?");
			ps.setInt(1, bookId);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static void getId(int cId) {
		// int status=0;
		try {
			Connection con = DB.getConnection();

			// status=updatebook(bookId);//updating quantity and issue

			PreparedStatement ps = con.prepareStatement("select id from librarian where name=?");
			ps.setString(1, UserLogin.CrntName);
			ResultSet rs = ps.executeQuery();
			rs.last();
			cId = rs.getInt(1);
			rs.beforeFirst();

			con.close();
		} catch (Exception e) {
			System.out.println(e);
			BuyBookDao.cId = cId;
		}
	}
}
