import java.sql.*;

public class OrderDao {

	public static int save(String delAddress,String delCity,String state,String postCode,String country){
		int status=0;
		String orderBook;
		String Tprice;
		try{
			Connection con=DB.getConnection();
			PreparedStatement ps1 = con.prepareStatement("select bookTitle,price from issuebooks where Id=?");
			ps1.setInt(1, ShopCartView.purchaseId);
			ResultSet rs = ps1.executeQuery();
			rs.last();
			orderBook = rs.getString(1);
			Tprice=rs.getString(2);
			rs.beforeFirst();
			PreparedStatement ps=con.prepareStatement("insert into Orders(customerName,bookTitle,delAddress,delCity,state,postCode,Country,TPrice) values(?,?,?,?,?,?,?,?)");
			
			ps.setString(1,UserLogin.CrntName);
			ps.setString(2,orderBook);
			ps.setString(3,delAddress);
			ps.setString(4,delCity);
			ps.setString(5,state);
			ps.setString(6,postCode);
			ps.setString(7,country);
			ps.setDouble(8,Double.parseDouble(Tprice));
			status=ps.executeUpdate();
			con.close();
		}catch(Exception e){System.out.println(e);}
		return status;
	}

	
}
