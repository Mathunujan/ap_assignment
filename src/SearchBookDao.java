import java.sql.*;

public class SearchBookDao {

	public static boolean checkBook(String bookTitle) {
		boolean status = false;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from books where Title=?");
			ps.setString(1, bookTitle);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	// ( int userId, String CustomerName,String Contact_no,int Qty,double price)
	public static int save(String bookTitle) {
		int status = 0;
		String priceP;
		Date CDate= null;
		//Date date =new Date();
		try {
			Connection con = DB.getConnection();

			status = updatebook(bookTitle);// updating quantity and issue
			PreparedStatement ps1 = con.prepareStatement("select price from books where title=?");
			ps1.setString(1, SearchBook1.bookcallno);
			ResultSet rs = ps1.executeQuery();
			rs.last();
			priceP = rs.getString(1);
			rs.beforeFirst();

			if (status > 0) {
				PreparedStatement ps = con.prepareStatement(
						"insert into issuebooks(BookTitle,CustomerId,CustomerName,Date_Time,Quantity,Price) values(?,?,?,?,?,?)");
				// PreparedStatement ps=con.prepareStatement(" create view ShoppingCart as
				// select librarian.id,books.Title from librarian,books where books.id=?;)
				// values(?,?)");
				ps.setString(1, bookTitle);
				ps.setString(2, "");
				ps.setString(3, UserLogin.CrntName);
				ps.setDate(4, CDate);
				ps.setLong(5, SearchView.qty);
				ps.setDouble(6, Double.parseDouble(priceP)*SearchView.qty);

				status = ps.executeUpdate();
			}

			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int updatebook(String bookcallno) {
		int status = 0;
		int quantity = 0, issued = 0;
		try {
			Connection con = DB.getConnection();

			PreparedStatement ps = con.prepareStatement("select quantity from books where Title=?");
			ps.setString(1, bookcallno);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				quantity = rs.getInt("quantity");
				// issued=rs.getInt("issued");
			}

			if (quantity > 0) {
				PreparedStatement ps2 = con.prepareStatement("update books set quantity=? where Title=?");
				ps2.setInt(1, quantity - 1);
				// ps2.setInt(2,issued+1);
				ps2.setString(2, bookcallno);

				status = ps2.executeUpdate();
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}
}
