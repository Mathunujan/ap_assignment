import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;

public class InvoiceOrder extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InvoiceOrder frame = new InvoiceOrder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InvoiceOrder() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(139, 40, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(26, 83, 86, 20);
		contentPane.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(139, 83, 86, 20);
		contentPane.add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(233, 83, 86, 20);
		contentPane.add(textField_3);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(139, 134, 86, 20);
		contentPane.add(textField_4);

		JButton btnGoToMain = new JButton("Go to main menu");
		btnGoToMain.setBounds(288, 202, 113, 23);
		contentPane.add(btnGoToMain);

		JButton btnPrint = new JButton("Print");
		btnPrint.setBounds(335, 11, 89, 23);
		contentPane.add(btnPrint);

		// textField.setText(UserLogin.CrntName);
		// textField_1.setText(UserLogin.CrntName);

		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con
					.prepareStatement("select CustomerName,BookTitle,Price,Quantity from issuebooks where id=?");
			ps.setInt(1, ShopCartView.purchaseId);
			ResultSet rs = ps.executeQuery();
			rs.last();
			textField.setText(rs.getString(1));
			textField_1.setText(rs.getString(2));
			textField_2.setText(rs.getString(3));
			textField_3.setText(rs.getString(4));
			double totalP = Double.parseDouble(rs.getString(3)) * Double.parseDouble(rs.getString(4));
			rs.beforeFirst();
			textField_4.setText(String.valueOf(totalP));
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
