import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ShopCartView extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	static int purchaseId;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShopCartView frame = new ShopCartView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShopCartView() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 703, 411);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		String data[][] = null;
		String column[] = null;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from issuebooks where CustomerName=?",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ps.setString(1, UserLogin.CrntName);
			ResultSet rs = ps.executeQuery();

			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			column = new String[cols];
			for (int i = 1; i <= cols; i++) {
				column[i - 1] = rsmd.getColumnName(i);
			}

			rs.last();
			int rows = rs.getRow();
			rs.beforeFirst();

			data = new String[rows][cols];
			int count = 0;
			while (rs.next()) {
				for (int i = 1; i <= cols; i++) {
					data[count][i - 1] = rs.getString(i);
				}
				count++;
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		contentPane.setLayout(null);

		table = new JTable(data, column);
		JScrollPane sp = new JScrollPane(table);
		sp.setBounds(5, 5, 540, 233);

		contentPane.add(sp);

		JButton btnBuy = new JButton("Buy");
		btnBuy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 purchaseId = Integer.parseInt(textField.getText());
				
				// int i=BuyBookDao.delete(bookId, BuyBookDao.cId );
				if (purchaseId > 0) {
					if (BuyBookDao.updatebook(purchaseId)) {
						JOptionPane.showMessageDialog(ShopCartView.this,
								"this item process to purchasing \n please select payment method!");
						PayMethod.main(new String[] {});
						// frame.dispose();

					} else {
						JOptionPane.showMessageDialog(ShopCartView.this, "Sorry, unable to issue!");
					} // end of save if-else

				} else {
					JOptionPane.showMessageDialog(ShopCartView.this, "Sorry, Book title doesn't exist!");
				} // end of checkbook if-else

			}

		});
		btnBuy.setBounds(456, 242, 89, 23);
		contentPane.add(btnBuy);

		textField = new JTextField();
		textField.setBounds(360, 243, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblEnterPurchaseId = new JLabel("Enter purchase Id");
		lblEnterPurchaseId.setBounds(243, 246, 115, 14);
		contentPane.add(lblEnterPurchaseId);
	}
}
