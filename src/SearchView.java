import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class SearchView extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	static int qty;
	public static JTextField textField_1;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchView frame = new SearchView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SearchView() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 748, 448);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		String data[][] = null;
		String column[] = null;
		try {
			Connection con = DB.getConnection();
			/*
			 * String priceP = null; textField_1.setText(priceP); PreparedStatement ps1 =
			 * con.prepareStatement("select price="+priceP+" from books where title=?");
			 * ps1.setString(1, SearchBook1.bookcallno);
			 */
			PreparedStatement ps = con.prepareStatement("select * from books where title=?",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ps.setString(1, SearchBook1.bookcallno);

			ResultSet rs = ps.executeQuery();

			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			column = new String[cols];
			for (int i = 1; i <= cols; i++) {
				column[i - 1] = rsmd.getColumnName(i);
			}

			rs.last();
			int rows = rs.getRow();
			rs.beforeFirst();

			data = new String[rows][cols];
			int count = 0;
			while (rs.next()) {
				for (int i = 1; i <= cols; i++) {
					data[count][i - 1] = rs.getString(i);
				}
				count++;
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		contentPane.setLayout(null);

		table = new JTable(data, column);
		JScrollPane sp = new JScrollPane(table);
		sp.setBounds(0, 11, 712, 134);

		contentPane.add(sp);

		JButton btnAddToShopping = new JButton("Add to shopping cart");
		btnAddToShopping.setBounds(564, 151, 141, 23);
		btnAddToShopping.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String bookcallno = SearchBook1.bookcallno;

				qty = Integer.parseInt(textField.getText());
				if (qty > 0) {
					if (SearchBookDao.checkBook(bookcallno)) {

						int i = SearchBookDao.save(bookcallno);
						if (i > 0) {
							JOptionPane.showMessageDialog(SearchView.this, "Book added successfully!");
							ShopCartView.main(new String[] {});
							// frame.dispose();

						} else {
							JOptionPane.showMessageDialog(SearchView.this, "Sorry, unable to issue!");
						} // end of save if-else

					} else {
						JOptionPane.showMessageDialog(SearchView.this, "Sorry, Book title doesn't exist!");
					} // end of checkbook if-else

				} else {
					JOptionPane.showMessageDialog(SearchView.this, "Sorry, you must enter the quantity atleast 1!");
				}
			}
		});
		contentPane.add(btnAddToShopping);

		JButton btnShowMyShpping = new JButton("Show my shopping cart");
		btnShowMyShpping.setBounds(53, 156, 186, 23);
		btnShowMyShpping.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ShopCartView.main(new String[] {});
			}
		});
		contentPane.add(btnShowMyShpping);

		textField = new JTextField();
		textField.setBounds(468, 152, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(326, 152, 86, 20);
		contentPane.add(textField_1);
	}
}
